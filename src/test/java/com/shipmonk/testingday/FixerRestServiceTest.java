package com.shipmonk.testingday;

import rest.ExchangeRateResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import rest.FixerRestService;
import rest.RestService;

public class FixerRestServiceTest {
    @Test
    public void test() {
        RestService restService = new FixerRestService();
        ResponseEntity<ExchangeRateResponse> responseEntity = restService.getExchangeRates("USD", "2022-02-02");
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody().getSuccess());
        System.out.println(responseEntity.getBody().getRates());
    }
}

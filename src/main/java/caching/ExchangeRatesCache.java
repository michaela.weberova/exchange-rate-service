package caching;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import rest.ExchangeRateResponse;
import rest.RestService;

import java.util.Date;

public class ExchangeRatesCache {

    @Autowired
    RestService restService;

    public ExchangeRateResponse getExchangeRates(Date date) {
        ResponseEntity<ExchangeRateResponse> responseEntity = restService.getExchangeRates("USD", date.toString());
        return responseEntity.getBody();
    }
}

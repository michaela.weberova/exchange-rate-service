package com.shipmonk.testingday;

import caching.ExchangeRatesCache;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rest.ExchangeRateResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping(
    path = "/api/v1/rates"
)
public class ExchangeRatesController
{
    ExchangeRatesCache cache;

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    @RequestMapping(method = RequestMethod.GET, path = "/{day}")
    public ResponseEntity<Object> getRates(@PathVariable("day") String day)
    {
        ExchangeRateResponse rates;

        try {
            DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            dateFormat.setLenient(false);
            Date date = dateFormat.parse(day);
            rates = cache.getExchangeRates(date);
        }
        catch (ParseException e) {
            return new ResponseEntity<>(
                "Invalid date format.",
              HttpStatus.BAD_REQUEST
            );
        }

        if(rates == null) {
            return new ResponseEntity<>(
                HttpStatus.NOT_FOUND
            );
        }

        return new ResponseEntity<>(
            rates,
            HttpStatus.OK
        );
    }
}

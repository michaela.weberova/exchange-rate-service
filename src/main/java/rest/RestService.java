package rest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public interface RestService {
    ResponseEntity<ExchangeRateResponse> getExchangeRates(String baseCurrency, String date);
}

package rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class FixerRestService implements RestService {

    String url = "https://api.apilayer.com/fixer/";

    @Override
    public ResponseEntity<ExchangeRateResponse> getExchangeRates(String baseCurrency, String date) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);
        httpHeaders.set("apikey", "WBCSXp8g9ej468xzCQw2OY37ye8PqODY");
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(url)
            .pathSegment(date)
            .queryParam("base", baseCurrency);
        URI uri = uriComponentsBuilder.build().toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, entity, ExchangeRateResponse.class);
    }
}

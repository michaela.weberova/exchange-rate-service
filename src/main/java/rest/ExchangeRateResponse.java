package rest;

import java.util.HashMap;

public class ExchangeRateResponse {
    private Boolean success;

    private Integer timestamp;

    private String base;

    private String date;

    private HashMap<String, Double> rates;

    public HashMap<String, Double> getRates() {
        return rates;
    }

    public void setRates(HashMap<String, Double> rates) {
        this.rates = rates;
    }

    public Boolean getSuccess() {
        return success;
    }

}

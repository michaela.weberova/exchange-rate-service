package model;

import javax.persistence.*;
import java.util.HashMap;

@Entity
public class ExchangeRates {
    @Id
    @GeneratedValue
    String id;

    String baseCurrency;

    @ElementCollection
    HashMap<String, Double> exchangeRates;
}
